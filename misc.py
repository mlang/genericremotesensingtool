# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 14:25:10 2018

@author: marc lang
"""


import otbApplication as otb
from ymraster import RasterDataType
#from mes_fonctions import gdal_translate





def bandmathx(list_in_filename, out_filename, expr, out_dtype):

    dtype = RasterDataType(lstr_dtype=out_dtype).otb_dtype
    # Apply bandMathx application
    BandMath = otb.Registry.CreateApplication("BandMathX")
    BandMath.SetParameterStringList("il", list_in_filename)
    BandMath.SetParameterString("out", out_filename)
    BandMath.SetParameterString("exp", expr)
    BandMath.SetParameterOutputImagePixelType("out", dtype)
    BandMath.ExecuteAndWriteOutput()

    # write new nodata value in metadata

def majority_filter(*args,**kwargs):
    """
    This function returns an image filtered by a majority filter. This is an
    adapted version of the Orfeo Toolbox "ClassificationMapRegularization"
    application. See https://www.orfeo-toolbox.org/CookBook/CookBooksu114.html
    for more details.

    Parameters
    ----------
    name_file : string
        Path of the image file
    out_filename : string
        Path of the filtered image
    radius : int, optional
        The radius of the ball shaped structuring element (expressed in pixels)
        . Defaults to 1.
    otb_dtype : string or int, optional
        Type of the output data, in OTB convention. Default to
        otb.ImagePixelType_uint8, i.e. 0 or 'uint8'
    """
    #Initialize parameters
    name_file = args[0]
    out_filename = args[1]
    radius = kwargs.get('radius',1)
    otb_dtype = kwargs.get('otb_dtype', otb.ImagePixelType_uint8)

    #Use the otb application
    maj_filter = otb.Registry.CreateApplication("ClassificationMapRegularization")
    maj_filter.SetParameterString("io.in", name_file)
    maj_filter.SetParameterString("io.out", out_filename)
    maj_filter.SetParameterInt("ip.radius", radius)
    maj_filter.SetParameterOutputImagePixelType("io.out", otb_dtype)
    maj_filter.ExecuteAndWriteOutput()