# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 15:06:47 2018

@author: marc lang
"""

from osgeo import ogr, osr
import os
from .extract_information import get_geographic_coord_from_points
from ymraster import Raster


def create_square_buffer(point_vector, buffer_size, field_name=None,
                         outShapefile=None):
    """
    Create and save a square buffer around points (EPSG : 2154).
    Parameters
    ----------
    point_vector : str
        Path of the input vector.
    outShapefile : str, optional
        Path of the output buffer. Defaut name is used if not indicated.
    buffer_size : int
        'Radius' of the buffer in meters. In this case, half-side of the
        square.
    field_name : str, optional
        Name of the field of point_vector contaning the id of the point.
    """

    # Get point geographic coordinates
    coord = get_geographic_coord_from_points(
        point_vector, field_name=field_name)

    # Save extent to a new Shapefile
    if not outShapefile:
        outShapefile = os.path.splitext(
            point_vector)[0] + '_square_buffer_{}.shp'.format(buffer_size)
    outDriver = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)

    # Create the output shapefile
    outDataSource = outDriver.CreateDataSource(outShapefile)
    outLayer = outDataSource.CreateLayer(
        "states_extent", geom_type=ogr.wkbPolygon)

    # Add an ID field
    idField = ogr.FieldDefn("id_point", ogr.OFTString)
    outLayer.CreateField(idField)

    for key, point in list(coord.items()):
        # Initiate the polygon
        poly = ogr.Geometry(ogr.wkbPolygon)
        # Get the extent of the buffer for each point (xmin, xmax, ymin, ymax)
        extent = (point[0] - buffer_size, point[0] + buffer_size,
                  point[1] - buffer_size, point[1] + buffer_size)
        # print extent[1]-extent[0], extent[3]-extent[2]

        # Create a Polygon for each point
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(extent[0], extent[2])
        ring.AddPoint(extent[1], extent[2])
        ring.AddPoint(extent[1], extent[3])
        ring.AddPoint(extent[0], extent[3])
        ring.AddPoint(extent[0], extent[2])
        poly.AddGeometry(ring)

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        feature = ogr.Feature(featureDefn)
        feature.SetGeometry(poly)
        feature.SetField("id_point", key)
        outLayer.CreateFeature(feature)
        feature = None

    # Save and close DataSource
    outDataSource = None

    # Create .qpj and .prj projection files
    # open file for writing
    qpj = open(os.path.splitext(outShapefile)[0] + '.qpj', 'w')
    text_to_add = ('PROJCS["RGF93 / Lambert-93",GEOGCS["RGF93", ' +
                   'DATUM["Reseau_Geodesique_Francais_1993",' +
                   'SPHEROID["GRS 1980",6378137,298.257222101,' +
                   'AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],' +
                   'AUTHORITY["EPSG","6171"]],PRIMEM["Greenwich",0,' +
                   'AUTHORITY["EPSG","8901"]],' +
                   'UNIT["degree",0.0174532925199433,' +
                   'AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4171"]],' +
                   'PROJECTION["Lambert_Conformal_Conic_2SP"],' +
                   'PARAMETER["standard_parallel_1",49],' +
                   'PARAMETER["standard_parallel_2",44],' +
                   'PARAMETER["latitude_of_origin",46.5],' +
                   'PARAMETER["central_meridian",3],' +
                   'PARAMETER["false_easting",700000],' +
                   'PARAMETER["false_northing",6600000],' +
                   'UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],' +
                   'AXIS["Y",NORTH],AUTHORITY["EPSG","2154"]]\n')
    qpj.write(text_to_add)
    qpj.close()

    prj = open(os.path.splitext(outShapefile)[0] + '.prj', 'w')
    text_to_add = ('PROJCS["RGF93_Lambert_93",GEOGCS["GCS_RGF93",' +
                   'DATUM["D_RGF_1993",' +
                   'SPHEROID["GRS_1980",6378137,298.257222101]],' +
                   'PRIMEM["Greenwich",0],' +
                   'UNIT["Degree",0.017453292519943295]],' +
                   'PROJECTION["Lambert_Conformal_Conic"],' +
                   'PARAMETER["standard_parallel_1",49],' +
                   'PARAMETER["standard_parallel_2",44],' +
                   'PARAMETER["latitude_of_origin",46.5],' +
                   'PARAMETER["central_meridian",3],' +
                   'PARAMETER["false_easting",700000],' +
                   'PARAMETER["false_northing",6600000],UNIT["Meter",1]]')
    prj.write(text_to_add)
    prj.close()


def create_grid_lines(output_grid, ref_raster, cell_size, epsg=2154):
    """
    Create a shapefile grid with the same extent than a reference raster file

    Paremeters
    ----------
    output_grid : string
        Path of output shapefile grid
    ref_raster : string
        Path of input reference raster file.
    cell_size : float
        Size of elementary grid cells in projection unit.
    epsg : int,
        EPSG code. Default to Lambert 93 projection (2154).
    """

    rst = Raster(ref_raster)
    xmin, xmax, ymin, ymax = rst.extent

    # get rows
    rows = (ymax - ymin) // cell_size
    # get columns
    cols = (xmax - xmin) // cell_size

    # create output file
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    outDriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(output_grid):
        os.remove(output_grid)
    outDataSource = outDriver.CreateDataSource(output_grid)
    outLayer = outDataSource.CreateLayer(output_grid, srs,
                                         geom_type=ogr.wkbLineString)
    featureDefn = outLayer.GetLayerDefn()

    countrows = 0
    y = ymax
    while countrows < rows:
        countrows += 1
        line = ogr.Geometry(ogr.wkbLinearRing)
        line.AddPoint(xmin, y)
        line.AddPoint(xmax, y)
        y -= cell_size

        # add new geom to layer
        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(line)
        outLayer.CreateFeature(outFeature)
        outFeature = None

    x = xmin
    countcols = 0
    while countcols < cols:
        countcols += 1
        line = ogr.Geometry(ogr.wkbLinearRing)
        line.AddPoint(x, ymax)
        line.AddPoint(x, ymin)
        x += cell_size

        # add new geom to layer
        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(line)
        outLayer.CreateFeature(outFeature)
        outFeature = None

    # Save and close DataSources
    outDataSource = None


def create_grid_cells(output_grid, ref_raster, cell_size, epsg=2154):
    """
    Create a shapefile grid with the same extent than a reference raster file

    Paremeters
    ----------
    output_grid : string
        Path of output shapefile grid
    ref_raster : string
        Path of input reference raster file.
    cell_size : float
        Size of elementary grid cells in projection unit.
    epsg : int,
        EPSG code. Default to Lambert 93 projection (2154).
    """

    rst = Raster(ref_raster)
    xmin, xmax, ymin, ymax = rst.extent

    # get rows
    rows = (ymax - ymin) // cell_size
    # get columns
    cols = (xmax - xmin) // cell_size

    # start grid cell envelope
    ringXleftOrigin = xmin
    ringXrightOrigin = xmin + cell_size
    ringYtopOrigin = ymax
    ringYbottomOrigin = ymax - cell_size

    # create output file
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    outDriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(output_grid):
        os.remove(output_grid)
    outDataSource = outDriver.CreateDataSource(output_grid)
    outLayer = outDataSource.CreateLayer(output_grid, srs,
                                         geom_type=ogr.wkbPolygon)
    featureDefn = outLayer.GetLayerDefn()

    # create a field
    fieldName = 'id'
    fieldType = ogr.OFTInteger
    idField = ogr.FieldDefn(fieldName, fieldType)
    outLayer.CreateField(idField)

    # create grid cells
    countcols = 0
    while countcols < cols:
        countcols += 1

        # reset envelope for rows
        ringYtop = ringYtopOrigin
        ringYbottom = ringYbottomOrigin
        countrows = 0

        while countrows < rows:
            current_id = cols * countrows + (countcols - 1)
            countrows += 1

            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)

            # add new geom to layer
            outFeature = ogr.Feature(featureDefn)
            outFeature.SetGeometry(poly)
            outFeature.SetField('id', current_id)
            outLayer.CreateFeature(outFeature)
            outFeature = None

            # new envelope for next poly
            ringYtop = ringYtop - cell_size
            ringYbottom = ringYbottom - cell_size

        # new envelope for next poly
        ringXleftOrigin = ringXleftOrigin + cell_size
        ringXrightOrigin = ringXrightOrigin + cell_size

    # Save and close DataSources
    outDataSource = None


def create_grid(output_grid, ref_raster, cell_size, epsg=2154, mode='cells'):
    """
    Create a shapefile grid with the same extent than a reference raster file

    Paremeters
    ----------
    output_grid : string
        Path of output shapefile grid
    ref_raster : string
        Path of input reference raster file.
    cell_size : float
        Size of elementary grid cells in projection unit.
    epsg : int,
        EPSG code. Default to Lambert 93 projection (2154).
    mode : str <'cells' or 'lines'>
        If cells, the output grid is polygon shapefile where entities are cells.
        If lines,  the output grid is a line shapefile where entities lines.
    """
    _list_mode = {'cells': create_grid_cells,
                  'lines': create_grid_lines}
    try:
        _list_mode[mode](output_grid, ref_raster, cell_size, epsg=epsg)
    except KeyError:
        msg = '"mode" key arg must be {}'.format(' or '.join(_list_mode.keys()))
        raise KeyError(msg)
