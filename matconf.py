# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 11:10:19 2016

@author: marc lang
"""

import function_prairie as fp
import scipy as sp
from sklearn.metrics import confusion_matrix
import otbApplication
from ymraster import RasterDataType, Raster, write_file
import os
from tabulate import tabulate
from rasterstats import zonal_stats
import matplotlib.pyplot as plt

class Indices_to_latex():
    """
    """

    def __init__(self, cm, indices, headers,
                 nb_pix_by_class):
        self.cm = cm
        self.indices = indices
        self.headers = headers
        self.n_classe = self.cm.shape[0]
        self.n_col = self.n_classe + 3
        self.nb_pix_by_class = nb_pix_by_class

    def _third_line(self,):
        """ """
        line = ''
        for h in self.headers:
            line += '& {} '.format(h)
        line =  'Reference data ' + line +  '& Producer accuracy & Number of pixels \\\\ \n'
        line += ('\\cmidrule(lr){{1-1}}' +
                 '\\cmidrule(lr){{2-{}}}' +
                 '\\cmidrule(lr){{{}-{}}}\n').format(self.n_classe + 1,
                                                     self.n_col - 1,
                                                     self.n_col)

        return line

    def _write_header(self):
        """"""
        WIDTH = '1.5cm'
        header = ''
        header += '\\begin{tabular}'
        header += '{{l{}p{{{}}} p{{{}}}}} \\toprule\n'.format('r'*self.n_classe,
                                                              WIDTH,WIDTH)
        header += '& \\multicolumn{{{}}}{{c}}{{Classification results}}            \\\\ \\cmidrule{{2-{}}}\n'.format(self.n_col - 1,self.n_col)
        header += self._third_line()
        return header

    def _write_a_middle_row(self,idx):
        """"""
        row = ''
        row += '{}'.format(self.headers[idx])
        if isinstance(self.cm[0,0], int ):
            for i, elem in enumerate(self.cm[idx, :]):
                if i == idx:
                    row += ' & \\textbf{{{}}} '.format(elem)
                else:
                    row += ' & {} '.format(elem)
        else :
            for i, elem in enumerate(self.cm[idx, :]):
                if i == idx:
                    row += ' & \\textbf{{{:.2f}}} '.format(elem)
                else:
                    row += ' & {:.2f} '.format(elem)
        row += ' & {:.2f} '.format(self.indices[3][idx])
        row += ' & {}  \\\\\n'.format(self.nb_pix_by_class[idx])
        return row

    def _write_middle(self,):
        """
        """
        middle = ''
        for id_row in range(self.n_classe):
            middle += self._write_a_middle_row(id_row)
        middle += ('\\cmidrule(lr){{1-1}}' +
                   '\\cmidrule(lr){{2-{}}}\n').format(self.n_classe + 1)

        middle += '\\multicolumn{1}{r}{User acuracy}'
        for user_acc in self.indices[2]:
            middle += ' & {:.2f} '.format(user_acc)
        middle += ' & &    \\\\ \\bottomrule \n'

        return middle

    def _write_end(self,):
        """"""
        end = ''
        end += '\\multicolumn{{{n_col}}}{{l}}{{Overall Accuracy (OA) : {oa:.2f} ; KAPPA : {kappa:.2f}}}\n'.format(n_col = self.n_col,
                                                                                                                oa = self.indices[1],
                                                                                                                kappa = self.indices[0])
        end += '\end{tabular}\n'


        return end

    def write_latex(self,):
        """
        """
        latex = self._write_header()
        latex += self._write_middle()
        latex += self._write_end()

        return latex



def rasterization(in_vec, ref_img, out_img, field_name, nodata, dtype = None):
    """
    Rasterize a vector dataset using the otb application Rasterization.

    Parameters
    ----------
    in_vec : string,
        The input vector dataset to be rasterized.
    ref_img : string,
         A reference image from which to import output grid and projection
         reference system information.
    out_img : string,
        An output image containing the rasterized vector dataset.
    field_name : string,
        Pixels within a geometry will hold the value of a user-defined field
        extracted from this geometry.It is the name of the attribute field to
        burn
    nodata : int or float,
        Default value for pixels not belonging to any geometry.
    dtype : `RasterDataType`,
        Any data type supported by gdal, otb or numpy. By default, the
        data type is float otb type.
    """

    # The following line creates an instance of the Rasterization application
    Rasterization = otbApplication.Registry.CreateApplication("Rasterization")

    # The following lines set all the application parameters:
    Rasterization.SetParameterString("in", in_vec)
    Rasterization.SetParameterString("out", out_img)
    Rasterization.SetParameterString("im", ref_img)
    Rasterization.SetParameterString("mode", 'attribute')
    Rasterization.SetParameterString("mode.attribute.field", field_name)
    Rasterization.SetParameterFloat("background", nodata)
    if dtype:
        Rasterization.SetParameterOutputImagePixelType("out", dtype.otb_dtype)

    # The following line execute the application
    Rasterization.ExecuteAndWriteOutput()


def regroup_class_in_cm(cm, id_in, id_out):
    """
    Parameters
    ----------
    cm : numpy array, [n_class * n_class],
        Confusion matrix, reference in row and prediction in columns
    id_in : int,
        Index of row (or col) of target class.
    id_out, int
        Index of row (or col) of the class to regroup with the id_in class.

    Returns
    -------
    new_cm : numpy array, [n_class - 1 * n_class - 1],
        Confusion matrix, reference in row and prediction in columns

    """
    dim = cm.shape[0]

    cm[id_in, id_in] = cm[id_in, id_in] + cm[id_out, id_out]
    for i in range(dim):
        cm[id_in, i] = cm[id_in, i] + cm[id_out, i]
        cm[i,id_in] = cm[i, id_in] + cm[i, id_out]

    new_cm = sp.delete(cm, id_out, axis=1)
    new_cm = sp.delete(new_cm, id_out, axis=0)

    return new_cm

def compute_global_quality(mat_conf):
    """
    Compute quality indices from a confusion matrix.

    Parameters
    ----------
    mat_conf : 2d array [n_class * n_class],
        Confusion matrix, reference in row and prediction in columns

    Returns
    -------
    Kappa : float,
        The kappa index.
    total_acc : float,
        The overall accuracy of the classification.
    user_acc : array of float [n_class],
        User accuracy for each class.
    prod_acc : array of float [n_class],
        Producer accuracy for each class.
    prop_samp : array of float [n_class],
        Proportion of reference pixels for each reference class.
    prop_classif : array of float [n_class],
        Proportion of reference pixels for each predicted class.
    """

    user_acc = []
    prod_acc = []
    prop_samp = []
    prop_classif = []
    total_acc = 0

    # scan the diagonal elements (overall accuracy) and the rows (producer
    # accuracy) and compute the proportion of each reference class
    for i,row in enumerate(mat_conf):
        prod_acc.append(float(row[i]) / sum(row)) if sum(row) != 0 \
            else prod_acc.append(0.)
        prop_samp.append(float(sum(row)))
        total_acc += float(row[i])

    prop_samp /= sp.sum(mat_conf)
    total_acc /= sp.sum(mat_conf)

    #scan the diagonal elements and the columns (user
    #accuracy) and compute the proportion of each predicted class
    for i, col in enumerate(sp.transpose(mat_conf)):
        user_acc.append(float(col[i])/sum(col)) if sum(col) != 0 \
            else user_acc.append(0.)
        prop_classif.append(float(sum(col)))

    prop_classif /= sp.sum(mat_conf)

    #Computation of Kappa index
    prob_rand = 0
    for p_classif, p_samp in zip(prop_classif,prop_samp):
        prob_rand = prob_rand + (p_classif * p_samp)

    kappa = (total_acc - prob_rand)/(1 - prob_rand)



    return kappa , total_acc, sp.asarray(user_acc),sp.asarray(prod_acc), \
            sp.asarray(prop_samp), sp.asarray(prop_classif)

def normalize_confusion_matrix(mat_conf, normalize_type = 'reference'):
    """
    Paremeters
    ---------
    mat_conf : 2d array [n_class * n_class],
        Confusion matrix, reference in row and prediction in columns
    normalize_type : string {'reference', 'classification'}, Optional
        If reference, results are normalized according to references data,
        ortherwise according to classification data.

    Returns
    -------
    cm_norm : 2d array [n_class * n_class],
        Normalized confusion matrix, reference in row and prediction in columns
    """

    if normalize_type == 'reference':
        axis = 1
    elif normalize_type == 'classification':
        axis = 0

    cm_norm = mat_conf.astype('float') / mat_conf.sum(axis = axis,
                                                     keepdims = True)

    return cm_norm
def map_error(in_cla, in_samp, out_filename, dict_samp = None):
    """
    Compute a map error of classification with to respect to validation data
    with the corresponding look-up table. The numeric count (NC) of the output
    file is equal to x.y with x the value of validation data and y the
    classification.


    Parameters
    ----------
    in_cla : string,
        Path of the classication raster.
    in_samp : string,
        Path of the validation data raster.
    out_filename: string,
        Path of the output map error. The look-up table will have the same name
    dict_samp : dict ``{samp_value : cla_value}``, (optional)
        In case the validation data and the classification does not have the
        same Numeric Count for classes, a dictionnary can be provided to
        indicate the correspondences between validation  data (`samp_value`) and
        classification data (`cla_value`)
    """
    #extract array from image
    cla = Raster(in_cla)
    samp = Raster(in_samp)
    tab_cla = cla.array_from_bands(1, mask_nodata = False)
    tab_samp = samp.array_from_bands(1, mask_nodata = False)
    #create the output array
    dtype = RasterDataType(lstr_dtype = 'float32')
    tab_result = sp.zeros((samp.height, samp.width), dtype = dtype.numpy_dtype)

    #loop on numeric count of the classification
    temp = sp.unique(tab_samp)
    values = sp.extract( temp >=1 , temp)
    del temp
    dict_samp = {val : val for val in values} if not dict_samp else dict_samp
    with open(os.path.splitext(out_filename)[0] + '.txt', 'w') as txt:
        txt.write('0,0,0,0,0,0\n')
        for key, val1 in dict_samp.iteritems() :    #in validation data array
            for val2 in dict_samp.values(): #in classification array
                #val1 = dict_samp[val1]
                count = float(str(val1) + '.' + str(val2))
                tab_result[(tab_samp == key) & (tab_cla == val2)] = count
                line = '{0},255,0,0,255,{0}\n'.format(count) if val1 != val2 \
                        else '{0},0,255,0,255,{0}\n'.format(count)
                txt.write(line)
        txt.close()
    write_file(out_filename, array = tab_result, dtype = dtype,
               nodata_value = 0, srs = samp.srs, transform = samp.transform,
               )


def fancy_result(confusion_matrix, roi_file, classif_file, classes = None):
    '''
    Permit to print fancy result of classification in markdown format

    Parameters
    ----------
    confusion_matrix : array,
        Confusion matrix, with reference in rows et predictions in columns
    roi_file : string,
        Path of the file containing the validation data
    classif_file : string,
        Path of the classification file
    classes : list of string, (optional)
        Labels of the classes

    Returns
    -------
    fancy_print : string,
        String containing the formated results.
    '''
    #compute indices
    indices = compute_global_quality(confusion_matrix)

    #format the input
    headers = classes if classes else ()
    index =  ['**' + h +'**' for h in headers ] if classes else False
    n_class = confusion_matrix.shape[0]
    result_class = sp.concatenate(indices[2:]).reshape(4,n_class)
    index_class = ['**user acc**', '**prod acc**', '**prop samp**', '**prop classif**']
    headers_global = ['Kappa', 'OA']
    roi_file = os.path.split(roi_file)[1]
    classif_file = os.path.split(classif_file)[1]

    #print the input data
    fancy_print = '\n\n**INPUT validation data** : {}\n\n'.format(roi_file)
    fancy_print += '**INPUT classification raster** : {}\n\n'.format(classif_file)
    #print the global precision indices
    fancy_print += '\n\n**Global precicision**\n\n'
    fancy_print += tabulate([indices[:2]], headers = headers_global,
                            showindex = False,tablefmt = 'grid')
    #print the confusion matrix
    fancy_print += '\n\n**Confusion Matrix**\n\n'
    fancy_print += tabulate(confusion_matrix, headers = headers,
                            showindex = index, tablefmt = 'grid')
    #print the class precision indices
    fancy_print += '\n\n**Class precicision**\n\n'
    fancy_print += tabulate(result_class, headers = headers,
                            showindex = index_class, tablefmt = 'grid')


    return fancy_print





def script():
    list_vec = ['/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.shp',
                '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_intersect_100.shp',
                ]
    list_ref_img = ['/home/lang/Documents/data/comp_data/drone/2016_06_07_Aumelas_Drone/export_marc/zone_1_VIS_MHC_classed_0.5_2.tif',
                    '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged_maj3.tif',
                    '/home/lang/Documents/data/texture/data_test/aumelas/classification/classif_broni/class_Max_Likelihood_proj_ok_maj3.tif'
                    ]
    list_roi  = ['/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.tif',
            '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_intersect_100.tif',
            '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_fitbroni.tif',
            ]
    vec = list_vec [0]
    ref_img = list_ref_img[2]
    roi = list_roi[2]
    dtype = RasterDataType('uint8')
    rasterization(vec,ref_img,roi,'num',0, dtype = dtype)

    #change Numeric Count in classif in order that classif and sample have the same one
    list_classif = ['/home/lang/Documents/data/comp_data/drone/2016_06_07_Aumelas_Drone/export_marc/zone_1_VIS_MHC_classed_0.5_2.tif',
                    '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged_maj3.tif',
                    ]
    rst = Raster(list_classif[1])
    dic_val = {2:3,3:4}
    out = '/home/lang/Documents/data/comp_data/drone/2016_06_07_Aumelas_Drone/export_marc/zone_1_VIS_MHC_classed_0.5_2_val_changed.tif'
    rst.change_value(dic_val,idx_band = 1, out_filename = out)

    synthesis = False
    list_roi  = ['/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.tif',
            '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_intersect_100.tif',
            '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_fitbroni.tif',
            ]
    roi = list_roi[1]
    list_param = [['/home/lang/Documents/data/comp_data/drone/2016_06_07_Aumelas_Drone/export_marc/zone_1_VIS_MHC_classed_0.5_2.tif',],
                  ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged.tif',{2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged_maj3.tif',{2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged.tif', {2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged_maj3.tif', {2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_2Classes_jb_500.tif', {1:50,2:50,3:200,4:200},['Soil and Herbs', 'Ligneous']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_2Classes_jb_500_maj3.tif',{1:50,2:50,3:200,4:200},['Soil and Herbs', 'Ligneous']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_100_valchanged.tif', {2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_100_valchanged_maj3.tif' , {2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/classif_broni/class_Max_Likelihood_proj_ok.tif',{},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/classif_broni/class_Max_Likelihood_proj_ok_maj3.tif',{},['BS', 'H', 'LL', 'HL']],
                  ]
    classif = list_param[3][0]
    fancy_print = '```eval_rst'
    if synthesis :
        glob = []
        index = []
        for classif, dict_val, headers in list_param[2:8:2]:
            for my_roi in list_roi[0:2]:
                samp = 'samp 100' if '100' in my_roi else 'all samp'
                if ('100' in classif) and ('100' not in my_roi):
                    pass
                else:
                    X,Y,_,_,_,_,_,_ = fp.get_samples_from_roi(classif, my_roi)
                    for init, end in dict_val.iteritems():
                        Y[Y==init] = end
                    #reference in row and prediction in columns
                    cm = confusion_matrix(Y,X)
                    #cm = cm[1:,1:]s]
                    indices = compute_global_quality(cm)
                    glob.append(indices[0:2])
                    index.append(samp + ' - ' + os.path.split(classif)[1])
                    #cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, sp.newaxi
        print(tabulate(glob, headers = ['Kappa', 'OA'], showindex = index,
                       tablefmt = 'grid'))
    else: # results in details
        for classif, dict_val, headers in list_param[2:8:2]:
            X,Y,_,_,_,_,_,_ = fp.get_samples_from_roi(classif, roi)
            for init, end in dict_val.iteritems():
                Y[Y==init] = end
            #reference in row and prediction in columns
            cm = confusion_matrix(Y,X)
            cm = cm[1:,1:] if cm.shape == (5,5) else cm
            #cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, sp.newaxis]
            #indices = compute_global_quality(cm)
            fancy_print += fancy_result(cm, roi, classif, classes = headers)
            fancy_print += '\n----'
        fancy_print += '\n```'
        print fancy_print

def script_eror_file():

    list_roi  = ['/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.tif',
            '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata_intersect_100.tif']
    roi = list_roi[0]
    list_param = [['/home/lang/Documents/data/comp_data/drone/2016_06_07_Aumelas_Drone/export_marc/zone_1_VIS_MHC_classed_0.5_2.tif',],
                  ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged.tif',{1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged_maj3.tif',{1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged.tif', {1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_jb_500_valchanged_maj3.tif', {1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_2Classes_jb_500.tif', {1:50,2:50,3:200,4:200},['Soil and Herbs', 'Ligneous']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_2Classes_jb_500_maj3.tif',{1:50,2:50,3:200,4:200},['Soil and Herbs', 'Ligneous']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_100_valchanged.tif', {1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_100_valchanged_maj3.tif' , {1:1,2:85,3:170,4:255},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/classif_broni/class_Max_Likelihood_proj_ok.tif',{},['BS', 'H', 'LL', 'HL']],
                    ['/home/lang/Documents/data/texture/data_test/aumelas/classification/classif_broni/class_Max_Likelihood_proj_ok_maj3.tif',{},['BS', 'H', 'LL', 'HL']],
                  ]

    for classif,dict_samp,_ in list_param[1:9]:
        fold = '/home/lang/Documents/data/texture/data_test/aumelas/classification/map_error'
        cla = os.path.split(classif)[1]
        name = os.path.splitext(cla)[0] + '_error_map' + os.path.splitext(cla)[1]
        error_file = os.path.join(fold, name)
        map_error(classif, roi, error_file, dict_samp = dict_samp)

def script_analyse_error_vs_lc():
    '''
    '''
    shape = '/home/lang/Documents/data/texture/data_test/aumelas/aumelas_test_grid_500p_o.shp'
    classif = '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged_maj3.tif'
    error_map = '/home/lang/Documents/data/texture/data_test/aumelas/classification/map_error/aumelas_test_orthoirc_cutted_NDVI_4Classes_500_valchanged_maj3_error_map.tif'
    stat = zonal_stats(shape, classif, categorical = True)
    stat2 = zonal_stats(shape, error_map, categorical = True)
    classes = [1,85,170,255]
    dict_lc = {i:[] for i in classes}
    dict_error = {i:[] for i in classes}
    sum_lc = sum(stat[0].values()) #number total amount of pixel in a windows, same for each window
    for lc, error in zip(stat, stat2):
        for cla in classes:
            #compute the % of classification error per class and per window
            n_digit = len(str(cla))
            error_keys = [val for val in error.keys() if int(val) == cla] #all ref pixel with cla value
            if error_keys :
                sum_cla = sum([error[e_key] for e_key in error_keys])
                good_key = None
                for e_key in error_keys:
                    cla_val = int(str(e_key).split('.')[1][:n_digit])
                    good_key = e_key if abs(cla_val - cla) < 5 else None
                n_cla_error = error[good_key] if good_key else 0
                dict_error[cla].append(1 - float(n_cla_error) / sum_cla)

                #compute the % of LC per class and per window
                n_cla_lc = lc.get(cla, 0)
                dict_lc[cla].append(float(n_cla_lc) / sum_lc)   #nb of pixel of class / nb total of pixel

    cla =255
    plt.scatter(dict_lc[cla],dict_error[cla], alpha = 0.5)
    plt.title('Pixel wrongly predicted for class {}'.format(cla))
    plt.xlabel('% of strata {}'.format(cla))
    plt.ylabel('Error in %')
    plt.plot()
    cla = 175
    plt.scatter(dict_lc[cla],dict_error[cla], alpha = 0.5)

    #plt.ylim(plt.ylim()[0] - 0.1, plt.ylim()[1] + 0.1)
    #plt.xlim(plt.xlim()[0] - 0.1, plt.xlim()[1] + 0.1)

def hist_band_per_classe():
    """
    """
    roi = '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.tif'
    img = '/home/lang/Documents/data/texture/data_test/aumelas/aumelas_test_orthoirc_cutted.tif'


    dict_cla = {1 : 'bare soil',
                2 : 'herbs',
                3 : 'low ligneous',
                4 : 'high ligneous',
                }
    n_col = 2
    fig, axes = plt.subplots(nrows= 2, ncols= n_col)
    fig.suptitle('Histogramme of reflectance')
    i = 0
    for cla, cla_lab in dict_cla.iteritems():
        r = i / n_col
        c = i % n_col
        X,_,_,_,_,_,_,_ = fp.get_samples_from_roi(img,roi, value_to_extract = cla)

        b1 = X[:,0] #infra ref
        b2 = X[:,1] #red
        b3 = X[:,2] #green
        label = ['infra-red', 'red', 'green']
        color = ['r','g', 'b']
        axes[r][c].hist([b1,b2,b3],bins = 1000, normed = True, histtype = 'step', color = color, alpha = 0.3, label = label)
        #axes[r][c].set_label(label)
        axes[r][c].legend()
        axes[r][c].set_ylabel('frequencies')
        axes[r][c].set_xlabel('reflectance')
        axes[r][c].set_title('{}'.format(cla_lab))
        #plt.show()
        i += 1

def hist_classes_per_band():
    """
    """
    roi = '/home/lang/Documents/data/texture/data_test/aumelas/sample_strata.tif'
    img = '/home/lang/Documents/data/texture/data_test/aumelas/aumelas_test_orthoirc_cutted.tif'


#    dict_cla = {1 : ['bare soil',[float(i) / 255 for i in (255,255,204)]],
#                2 : ['herbs',[float(i) / 255 for i in (169,219,142)]],
#                3 : ['low ligneous',[float(i) / 255 for i in (72,174,96)]],
#                4 : ['high ligneous',[float(i) / 255 for i in (0,104,55)]],
#                }
    dict_cla = {1 : ['bare soil',[float(i) / 255 for i in (50,50,50)]],
                2 : ['herbs','y'],
                3 : ['low ligneous',[float(i) / 255 for i in (169,219,142)]],
                4 : ['high ligneous','g'],
                }
    b1, b2, b3, label, color = [[] for i in range(5)]

    for cla, [cla_lab,cla_col] in dict_cla.iteritems():
        X,_,_,_,_,_,_,_ = fp.get_samples_from_roi(img,roi, value_to_extract = cla)
        b1.append(X[:,0])
        b2.append(X[:,1])
        b3.append(X[:,2])
        label.append(cla_lab)
        color.append(cla_col)

    ndvi = [(ir - r) / (ir + r) for ir, r in zip (b1,b2)]

    dict_band = {'infra-red' : b1,
                 'red' : b2,
                 'green' : b3,
                 'ndvi' : ndvi,
                }
    n_col = 2
    fig, axes = plt.subplots(nrows= 2, ncols= n_col)
    fig.suptitle('Histogramme of reflectance')
    i = 0
    for band_lab, band in dict_band.iteritems():
        r = i / n_col
        c = i % n_col
        axes[r][c].hist(band,bins = 255, normed = True, histtype = 'stepfilled', linewidth = 0, color = color, alpha = 0.6, label = label)
        #axes[r][c].set_label(label)
        axes[r][c].legend()
        axes[r][c].set_ylabel('frequencies')
        axes[r][c].set_xlabel('reflectance')
        axes[r][c].set_title('{}'.format(band_lab))
        #plt.show()
        i += 1

def hist_thres_ndvi():
    """
    """
    dict_thresh = {'broni 100' : '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/output_matlab/broni/aumelas_test_orthoirc_cutted_Threshold_NDVI_100.mat',
                   'broni 500' : '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/output_matlab/broni/aumelas_test_orthoirc_cutted_Threshold_NDVI_500.mat',
                   'jb 500' : '/home/lang/Documents/data/texture/data_test/aumelas/classification/assisted_classif/output_matlab/jb_500/aumelas_test_orthoirc_cutted_Threshold_NDVI_500.mat',
                    }
    n_col = 2
    fig, axes = plt.subplots(nrows= 2, ncols= n_col)
    thresh_lab = 'low vs high ligneous'
    fig.suptitle('Histogramme of treshold : {}'.format(thresh_lab))

    i = 0
    for th_title, th_path in dict_thresh.iteritems():

        mat = sp.io.loadmat(th_path)['Threshold_NDVI']
        th = mat[:,:,2].flatten()
        th = th[~sp.isnan(th)]
        r = i / n_col
        c = i % n_col
        axes[r][c].hist(th,bins = 255, normed = True, histtype = 'bar', color = 'b', alpha = 0.6)
        axes[r][c].set_ylabel('frequencies')
        axes[r][c].set_xlabel('ndvi threshold')
        title = th_title + ' : mean {}, std {}'.format(round(sp.mean(th),4),
                                                      round(sp.std(th),4))
        axes[r][c].set_title('{}'.format(title))
        i += 1




