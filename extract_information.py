# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 10:35:21 2017

@author: marc lang
"""

import scipy as sp
from osgeo import gdal, ogr
from tempfile import gettempdir
import os
import sys
from ymraster import Raster

import otbApplication

try:
    import otbApplication as otb

    OTB_IS_LOAD = True
    try:
        app = otb.Registry.CreateApplication('Smoothing')
        app.SetParameterString('out', 'foo.tif')
    except AttributeError:
        OTB_IS_LOAD = False
        ERROR_OTB = AttributeError(
            "This functionnality is not available without Orfeo Toolbox : "
            "Unable to create otbApplication objects\n\n"
            "Please set the ITK_AUTOLOAD_PATH environment variable "
            "to the Orfeo Toolbox applications folder path "
            "(usually something like '/usr/lib/otb/applications') ")
        # raise ImportError(MSG_ERROR_OTB)
except ImportError as e:
    OTB_IS_LOAD = False
    ERROR_OTB = ImportError(
        str(e) +
        "\n\nThis functionnality is not available without Orfeo Toolbox. " +
        "Please install Orfeo Toolbox if it isn't installed yet.\n\n"
        "Also, add the otbApplication module path "
        "(usually something like '/usr/lib/otb/python') "
        "to the PYTHONPATH environment variable.")


def otb_function(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        if not OTB_IS_LOAD:
            raise ERROR_OTB
        return f(*args, **kwds)

    return wrapper


@otb_function
def rasterization(in_vec, ref_img, out_img, nodata, field_name=None,
                  foreground_value=255, dtype=None, ram=None):
    """
    Rasterize a vector dataset using the otb application Rasterization.

    Parameters
    ----------
    in_vec : string,
        The input vector dataset to be rasterized.
    ref_img : string,
         A reference image from which to import output grid and projection
         reference system information.
    out_img : string,
        An output image containing the rasterized vector dataset.
    nodata : int or float,
        Default value for pixels not belonging to any geometry.
    foreground_value : int or string,
        The default mode : pixels within a geometry will hold the user-defined
        foreground value (by default 255). Ignored if field_name parameter is
        indicated.
    field_name : string, (optional)
        Pixels within a geometry will hold the value of a user-defined field
        extracted from this geometry.It is the name of the attribute field to
        burn.
    dtype : otbDataType,
        By default, the data type is float otb type.
    ram : int, (optional)
        Ram available in MB
    """

    # The following line creates an instance of the Rasterization application
    Rasterization = otbApplication.Registry.CreateApplication("Rasterization")

    # The following lines set all the application parameters:
    Rasterization.SetParameterString("in", in_vec)
    Rasterization.SetParameterString("out", out_img)
    Rasterization.SetParameterString("im", ref_img)

    if field_name:
        mode = 'attribute'
        mode_set_param = "mode.attribute.field"
        mode_param = field_name
    else:
        mode = 'binary'
        mode_set_param = "mode.binary.foreground"
        mode_param = str(foreground_value)

    Rasterization.SetParameterString("mode", mode)
    Rasterization.SetParameterString(mode_set_param, mode_param)
    Rasterization.SetParameterFloat("background", nodata)
    if dtype:
        Rasterization.SetParameterOutputImagePixelType("out", dtype)

    if ram:
        Rasterization.SetParameterInt("ram", ram)
    # The following line execute the application
    Rasterization.ExecuteAndWriteOutput()

def get_samples_from_roi(raster_name,roi_name, value_to_extract = None,
                         bands = None, **kwargs):
    '''
    The function get the set of pixel of an image according to an roi file
    (vector or raster). In case of raster format, both map should be of same
    size.

    Parameters
    ----------
    raster_name : string
        The name of the raster file, could be any file that GDAL can open
    roi_name : string
        The name of the thematic image: each pixel whose values is greater
        than 0 is returned
    value_to_extract : float, optional, defaults to None
        If specified, the pixels extracted will be only those which are equal
        this value. By, defaults all the pixels different from zero are
        extracted.
    bands : list of integer, optional, defaults to None
        The bands of the raster_name file whose value should be extracted.
        Indexation starts at 0. By defaults, all the bands will be extracted.
    foreground_value : int or string,
        The default mode : pixels within a geometry will hold the user-defined
        foreground value (by default 255). Ignored if field_name parameter is
        indicated.
    field_name : string, (optional)
        Pixels within a geometry will hold the value of a user-defined field
        extracted from this geometry.It is the name of the attribute field to
        burn.
    output_fmt : {`full_matrix`, `by_label` }, (optional)
        By default, the function returns a matrix with all pixels present in the
        ``roi_name`` dataset. With option `by_label`, a dictionnary
        containing as many array as labels present in the ``roi_name`` data
        set, i.e. the pixels are grouped in matrices corresponding to one label,
        the keys of the dictionnary corresponding to the labels. The coordinates
        ``t`` will also be in dictionnary format.


    Returns
    -------
    X : ndarray or dict of ndarra
        The sample matrix. A nXd matrix, where n is the number of referenced
        pixels and d is the number of variables. Each line of the matrix is a
        pixel.
    Y : ndarray
        the label of the pixel
    t : tuple or dict of tuple
        tuple of the coordinates in the original image of the pixels
        extracted
    nr : int
        Number of rows of the roi image
    nc : int
        Number of columns of the roi image
    d : int
        Number of band of the image, or the number of bands specified in the
        bands input parameter
    GeoTransform : Gdal Geotransform type
        Gdal Geotransform parameter
    Projection : Gdal Projectiontype
        Gdal Projection parameter
    '''

    ## Open data
    raster = gdal.Open(raster_name,gdal.GA_ReadOnly)
    rst = Raster(raster_name)
    if raster is None:
        print('Impossible to open '+raster_name)
        exit()

    # Check if is roi is raster or vector dataset
    is_vector = False
    try :
        roi = gdal.Open(roi_name,gdal.GA_ReadOnly)
    except RuntimeError:
        try:
            roi = ogr.Open(roi_name)
            is_vector = True
        except RuntimeError:
            print('Impossible to open '+ roi_name)
            exit()

    #if it is a vector, rasterize in a temp file the roi
    if is_vector:
        out_raster_temp = os.path.join(gettempdir(), 'roi_raster.tif')
        foreground_value = kwargs.get('foreground_value',255)
        field_name = kwargs.get('field_name', None)
        rasterization(roi_name, raster_name, out_raster_temp, nodata = 0,
                      foreground_value = foreground_value, field_name = field_name)
        roi = gdal.Open(out_raster_temp, gdal.GA_ReadOnly)

    if (raster.RasterXSize != roi.RasterXSize) or \
        (raster.RasterYSize != roi.RasterYSize):
        print('Images should be of the same size')
        print('Raster : {}'.format(raster_name))
        print('Roi : {}'.format(roi_name))
        exit()

    ## Get some information
    nc = roi.RasterXSize    #number of columnq
    nr = roi.RasterYSize    #number of rows
    d = raster.RasterCount  #number of bands
    GeoTransform = roi.GetGeoTransform()
    Projection = roi.GetProjection()

    if not bands:
        bands = list(range(d))
    else:
        d = len(bands)

    ## Initialize the output
    ROI = roi.GetRasterBand(1).ReadAsArray()
    if value_to_extract:
        t = sp.where(ROI == value_to_extract)
    else:
        t = sp.nonzero(ROI) #coord of where the samples are

    Y = ROI[t].reshape((t[0].shape[0],1)).astype('int32')

    del ROI
    roi = None # Close the roi file
    os.remove(out_raster_temp) if is_vector else None   # del temp rasterized file

    try:
        X = sp.empty((t[0].shape[0],d), dtype=rst.dtype.numpy_dtype)
    except MemoryError:
        print('Impossible to allocate memory: roi too large')
        exit()

    ## Load the data
    for i in bands:
        temp = raster.GetRasterBand(i+1).ReadAsArray()
        X[:,i] = temp[t]
    del temp
    raster = None # Close the raster file

    # Store data in a dictionnaries if indicated
    if kwargs.get('output_fmt') == 'by_label':
        labels = sp.unique(Y)
        dict_X = {}
        dict_t = {}
        for lab in labels:
            coord = sp.where(Y == lab)[0]
            dict_X[lab] = X[coord]
            dict_t[lab] = (t[0][coord], t[1][coord])

        return dict_X, Y, dict_t, nr, nc, d, GeoTransform, Projection
    else:
        return X, Y, t, nr, nc, d, GeoTransform, Projection

def write_data(outname, im, GeoTransform,Projection):
    '''
    The function write the image on the  hard drive.
    Input:
        outname: the name of the file to be written
        im: the image cube
        GeoTransform: the geotransform information
        Projection: the projection information
    Output:
        Nothing --
    '''
    nl = im.shape[0]
    nc = im.shape[1]
    if im.ndim == 2:
        d=1
    else:
        d = im.shape[2]

    driver = gdal.GetDriverByName('GTiff')
    dt = im.dtype.name
    # Get the data type
    if dt == 'bool' or dt == 'uint8':
        gdal_dt=gdal.GDT_Byte
    elif dt == 'int8' or dt == 'int16':
        gdal_dt=gdal.GDT_Int16
    elif dt == 'uint16':
        gdal_dt=gdal.GDT_UInt16
    elif dt == 'int32':
        gdal_dt=gdal.GDT_Int32
    elif dt == 'uint32':
        gdal_dt=gdal.GDT_UInt32
    elif dt == 'int64' or dt == 'uint64' or dt == 'float16' or dt == 'float32':
        gdal_dt=gdal.GDT_Float32
    elif dt == 'float64':
        gdal_dt=gdal.GDT_Float64
    elif dt == 'complex64':
        gdal_dt=gdal.GDT_CFloat64
    else:
        print('Data type non-suported')
        exit()

    dst_ds = driver.Create(outname,nc,nl, d, gdal_dt)
    dst_ds.SetGeoTransform(GeoTransform)
    dst_ds.SetProjection(Projection)

    if d==1:
        out = dst_ds.GetRasterBand(1)
        out.WriteArray(im)
        out.FlushCache()
    else:
        for i in range(d):
            out = dst_ds.GetRasterBand(i+1)
            out.WriteArray(im[:,:,i])
            out.FlushCache()
    dst_ds = None


def build_img(nb_rows, nb_col,d, coord, labels, out_filename, GeoTransform,
              Projection, dt = 'int16'):
    """
    The fonction rebuild a classified image from the labels of the classified
    pixels and their initial coordinates.

    Parameters
    ----------
    nb_rows : int
        Number of rows of the original image
    nb_col : int
        Number of columns of the original image
    d : int
        Third dimension of the image to build.
    coord : tuple of ndarrays
        Tuple of the coordinates of the pixels classified
    labels : array
        2d matix of pixels in row and features in col
    out_filename :  string
        Path of the output filename
    GeoTransform : Gdal Geotransform type
        Gdal Geotransform parameter
    Projection : Gdal Projectiontype
        Gdal Projection parameter
    dt : numpy data_type, optional
        Numpy data type parameter. Defaults to 'int16'
    """
    #initialization of the array
    img = sp.zeros((nb_rows,nb_col,d), dt)

    #Check if the label matrix does have the proper dimensions
    labels = labels.reshape((labels.shape[0],1))\
        if len(labels.shape) == 1 \
        else labels

    for i in range(d):
        #fill the matrix with the labels
        img[coord[0],coord[1],i] = labels[:,i]

    #the function write_data doesn't work in the case of an array representing
    #1 band but with three dimension (with actually only two useful). In this
    #case, the dimension is changed from 3 to 2.
    img = img[:,:,0] if d == 1 else img

    #write the file
    write_data(out_filename,img,GeoTransform,Projection)




def pixel_coord_2_pos(raster,coord_x,coord_y, mode = 'int'):
    rst = gdal.Open(raster)
    geotransform = rst.GetGeoTransform()
    origin_x = geotransform[0]
    origin_y = geotransform[3]
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]

    if mode == 'int':
        x = int((coord_x - origin_x) / pixel_width)
        y = int((coord_y - origin_y) / pixel_height)
    elif mode == 'absolute':
        x = (coord_x - origin_x) / pixel_width
        y = (coord_y - origin_y) / pixel_height
    #coordX = originX+pixelWidth*xOffset
    #coordY = originY+pixelHeight*yOffset
    return x, y

def get_geographic_coord_from_points(vector, field_name=None):
    """
    Get the geographic coordinates of a point vector file .
    Parameters
    ----------
    vector : str
        Path of the input vector file, any format supported by ogr.
    field_name : str, optional
        Field of the shapefile containing the id of each point.

    Return
    ------
    coord : list or dict
        If field_name is not indicated, list of coordinates (tuple) : [(x,y)].
        Else, dict of coordinates : {id_point : (x,y)}
    """
    # Get geographic coordinates of each point
    dataSource = ogr.Open(vector, 0)
    layer = dataSource.GetLayer()

    if not field_name:
        coord = []
    else:
        coord = {}
    for feature in layer:
        geom = feature.GetGeometryRef()
        pt = geom.Centroid().ExportToWkt()    # String
        coord_f = eval(','.join(pt.split(' ')[1:]))    # Transform into tuple
        if not field_name:
            coord.append(coord_f)
        else:
            coord[feature.GetField(field_name)] = coord_f
    driver = None
    return coord


def get_pixel_coord_from_points(vector, raster, field_name):
    """
    For points in a vecto, return the pixel localisation (row, col) on the
    given raster for each point located in the raster.

    Parameters
    ----------
    vector : str
        Path of the input point vector. All the points must be in the raster.
    field_name : str,
         Name of the field contaning the id of the point.
    raster : str
        Path of the input raster

    Return
    ------
    pix_cood : dict
        Dictionary of pixel coordinates on the raster. {id_point : (row,col)}.
        If the point is not in the raster, nothing is indicated.
    coord : dict
        Dictionary of geographic coordinates in the EPSG. {id_point : (x,y)}
    """

    # Get geographic coordinates of each point
    coord = get_geographic_coord_from_points(vector, field_name=field_name)

    # Get the pixel coordinate of each point on the raster
    raster_ = gdal.Open(raster)
    geotransform = raster_.GetGeoTransform()
    originX = geotransform[0]    # Upper left corner xcoordinate
    originY = geotransform[3]    # Upper left corner ycoordinate
    pixelWidth = geotransform[1]
    pixelHeight = - geotransform[5]

    # Get pix coord of each point. They are inverted regarding geographic
    # coordinates ((y, x) instead of (x, y)), in order to read the array.
    pix_coord = {}    #Dict of coordinates {id_of_point : (row, col)}
    for id_pt, coord_pt in list(coord.items()):
        x, y = coord_pt
        col = int((x - originX) / pixelWidth)
        row = int((originY - y) / pixelHeight)
        if col >= 0 and row >= 0:
            pix_coord[id_pt] = (row, col)    # Coord are inverted to read the array

    return pix_coord, coord



def extract_img_from_vector_extent(imgs, shp_file, buf = 0 , **kwargs):
    """

    Parameters
    ----------
    rescale_mod : string, {'img', 'subimg'}
        By default None
    dst : tuple of float or int (dstmin, dstmax)
    field_name
    """
    rescale_mod = kwargs.get('rescale_mod', None)
    if rescale_mod :
        dstmin, dstmax = kwargs.get('dst', (0,1))
    field_name =  kwargs.get('field_name', "")

    ogr_driver = ogr.GetDriverByName('ESRI Shapefile')
    datasource = ogr_driver.Open(shp_file,0)
    if not datasource :
        sys.exit('ERROR : can not open {}'.format(shp_file))
    layer = datasource.GetLayer()

    if not isinstance(imgs, list):
        imgs = [imgs]

    list_img = []
    list_limx = []
    list_limy = []
    list_field = []
    temp = gettempdir()

    for i,feat in enumerate(layer):
        geom = feat.GetGeometryRef()
        list_field.append(feat.GetField(field_name)) if field_name else None
        x_min, x_max, y_min, y_max = geom.GetEnvelope()
        #projwin = [x_min - buf, y_max + buf, x_max + buf , y_min - buf]

        for j, img in enumerate(imgs):

            out = os.path.join(temp,'feat_{}_img_{}.tif'.format(i,j))
            rst = Raster(img)
            buf_pix = int(buf/ rst.pixel_size)
            #print buf, rst.pixel_size
            x, y = pixel_coord_2_pos(img, x_min, y_max)
            x2, y2 = pixel_coord_2_pos(img, x_max, y_min)
            srcwin = [x - buf_pix,y - buf_pix, x2 - x + 1 + 2 * buf_pix, y2 - y + 1 + 2 * buf_pix]
            #srcwin = [x ,y , x2 - x + 1 , y2 - y + 1 ]

            sub_img = rst.array_from_bands(block_win = srcwin)


            if rescale_mod == 'img':
                srcmin = sp.percentile(rst.array_from_bands(),2)
                srcmax = sp.percentile(rst.array_from_bands(),98)
                # sub_img = rescale_fmap(sub_img,dstmin ,dstmax, srcmin = srcmin,
                #                       srcmax = srcmax)
#            elif rescale_mod == 'subimg':
#                sub_img = rescale_fmap(sub_img,dstmin ,dstmax,
#                                       percentile = (2,98))

            list_img.append(sub_img)


            xmin_feat, ymax_feat = pixel_coord_2_pos(img, x_min, y_max, mode = 'absolute')
            xmax_feat, ymin_feat = pixel_coord_2_pos(img, x_max, y_min, mode = 'absolute')

            limx = [x_feat - x  - 0.5 + buf_pix for x_feat in [xmin_feat,xmax_feat, xmax_feat, xmin_feat, xmin_feat]]
            limy = [y_feat - y - 0.5 + buf_pix for y_feat in [ymin_feat,ymin_feat, ymax_feat, ymax_feat, ymin_feat]]
            list_limx.append(limx)
            list_limy.append(limy)




            #gdal_translate(img, out, srcwin = srcwin)

    return list_img, list_limx, list_limy, list_field