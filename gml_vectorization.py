# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:53:59 2018

@author: marc lang
"""

import os
import subprocess
import ogr
from math import log10
import osr
import numpy as np


def create_gml_and_grid(seg_output, gridSize, epsg=2154):
    """
    Generate and save the GML file and the sub Shapefiles.
    Parameters
    ----------
    seg_output : string,
        Path of the input raster resulting from a segmentation.
    gridSize : tuple of integer. ex : (500,500)
        Dimensions of the sub-Shapefiles, in the EPSG's unit (meters for
        Lambert 93)
    epsg : integer, default 2154
        EPSG. 2154 for Lambert 93
    """

    create_gml(seg_output)
    in_vec = os.path.splitext(seg_output)[0] + '.gml'
    list_output = generateGridBasedSubsets(in_vec, gridSize, epsg=epsg)
    return list_output


def create_gml(seg_output):
    """
    Create and save a GML file + an XSD file.
    Parameters
    ----------
    seg_output : string,
        Path of the input raster resulting from a segmentation.
    """

    # Create GML
    sh = False
    polycmd = 'gdal_polygonize.py'
    seg_vec_output = os.path.splitext(seg_output)[0] + '.gml'
    seg_vec_layername = os.path.splitext(os.path.basename(seg_vec_output))[0]
    cmd = [polycmd, seg_output, '-f', 'GML', seg_vec_output,
           seg_vec_layername, 'DN']
    subprocess.call(cmd, shell=sh)


def cloneVectorDataStructure(ds_in, fname, ly=0, epsg=None):
    ds_in_ly = ds_in.GetLayer(ly)

    if epsg is None:
        srs = ds_in_ly.GetSpatialRef()
    else:
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(epsg)

    drv = ogr.GetDriverByName('ESRI Shapefile')
    ds_out = drv.CreateDataSource(fname)
    geom_type = ds_in_ly.GetLayerDefn().GetGeomType()
    out_name = os.path.splitext(os.path.basename(fname))[0]
    ds_out_ly = ds_out.CreateLayer(out_name, srs=srs, geom_type=geom_type)
    f = ds_in_ly.GetNextFeature()
    [ds_out_ly.CreateField(f.GetFieldDefnRef(i))
        for i in range(f.GetFieldCount())]

    return ds_out


def generateGridBasedSubsets(in_vec, gridSize, epsg=None):
    """
    Generate and save sub-Shapefiles from a GML file.

    Parameters
    ----------
    in_vec : GML file
    gridSize : tuple of integer ex : (500,500)
        Dimensions of the sub-Shapefiles, in the EPSG's unit (meters for
        Lambert 93)
    epsg : integer
        EPSG. 2154 for Lambert 93

    Returns
    -------
    output_list : list of string
        List of the sub-Shapefiles filenames.

    """

    ds_in = ogr.Open(in_vec)
    ds_in_ly = ds_in.GetLayer(0)
    ext = ds_in_ly.GetExtent()

    rect = []
    for x in np.arange(ext[0], ext[1], gridSize[0]):
        for y in np.arange(ext[2], ext[3], gridSize[1]):
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(x, y)
            ring.AddPoint(x + gridSize[0], y)
            ring.AddPoint(x + gridSize[0], y + gridSize[1])
            ring.AddPoint(x, y + gridSize[1])
            ring.AddPoint(x, y)
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
            rect.append(poly)

    ds_out = []
    NZ = int(log10(len(rect)) + 1)

    out_list = []

    for i in range(len(rect)):
        if epsg == 2154:
            out_list.append(os.path.splitext(in_vec)[0] + '_' +
                            str(gridSize[0]) + 'x' + str(gridSize[1]) +
                            'm_' + str(i + 1).zfill(NZ) + '.shp')
        else:
            out_list.append(os.path.splitext(in_vec)[0] + '_' +
                            str(gridSize[0]) + 'x' + str(gridSize[1]) +
                            '_' + str(i + 1).zfill(NZ) + '.shp')
        ds_out.append(cloneVectorDataStructure(ds_in, out_list[i], epsg=epsg))

    ds_in_ly.ResetReading()

    for f in ds_in_ly:
        i = 0
        found = False
        while not found:
            if f.GetGeometryRef().Intersects(rect[i]):
                ds_out[i].GetLayer(0).CreateFeature(f)
                found = True
            i += 1

    ds_out = None
    ds_in = None

    return out_list
