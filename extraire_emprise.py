# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 17:18:01 2016

@author: marc lang
"""
from ymraster import Raster, write_file
from RasterDataType import RasterDataType
import geopandas as gpd
import scipy as sp
import os
from tempfile import gettempdir


def define_mask(in_rst_filename, nodata, out_filename=None):
    """
    Create a binary mask from a raster, according to a specific nodata. All
    values that are different from nodata value are set to 1 and the others to
    0.

    Parameters
    ----------
    in_rst_filename : str,
        Path of the input raster image.
    nodata : float,
        Value that will be set to 0 in the mask.
    out_filename : str,(optional)
        Path of the output mask. If not indicate, the file will be created in
        a temp directory.

    Returns
    -------
    out_filename : str,
        The path of the output binary mask.
    """
    # get temp dir
    if not out_filename:
        out_filename = os.path.join(gettempdir(), 'mask.tif')

    in_rst = Raster(in_rst_filename)
    # Take all band into account if there are more than one
    if in_rst.count > 1:
        mask = sp.ones((in_rst.height, in_rst.width), dtype='int16')
        for i in range(in_rst.count):
            band = in_rst.array_from_bands(i + 1, mask_nodata=False)
            mask = mask * sp.where(band != nodata, 1, 0)
    else:
        band = in_rst.array_from_bands(mask_nodata=False)
        mask = sp.where(band != nodata, 1, 0).astype('uint8')

    # Write the mask file
    dtype = RasterDataType(lstr_dtype="uint8")
    write_file(out_filename, array=mask, dtype=dtype, nodata_value=0,
               srs=in_rst.srs, transform=in_rst.transform)

    return out_filename


def vectorization(in_rst_filename, out_filename=None,
                  ogr_format="ESRI Shapefile"):
    """
    Vectorize a raster file gdal_polygonize.py script.

    Parameters
    ----------
    in_rst_filename : str,
        Raster to polygonize
    out_filename : str, (optional)
        Path of the output vector file. If not indicate, the file will be
        created in a temp directory.
    ogr_format : str, (optional)
        Any vector format supported by gdal. Default to "ESRI Shapefile" format.

    Returns
    -------
    out_filename : string,
        The path of the output vector file.
    """
    in_rst = Raster(in_rst_filename)
    # get temp dir
    if not out_filename:
        out_filename = os.path.join(gettempdir(), 'mask.shp')

    # Carreful, the "" are necessary for gdal to understand the format
    my_call = 'gdal_polygonize.py {} -f "{}" {}'.format(in_rst.filename,
                                                        ogr_format,
                                                        out_filename)
    os.system(my_call)

    return out_filename


def extract_boundaries(in_shp, out_filename):
    """
    Extract from a vector layer all the entities that have a value of 1 and
    create a new vector file.

    Parameters
    ----------
    in_shp : string,
        Path of the input shapefile.
    out_filename : string,
        Path of the output boundarie vector file.
    """

    gdf = gpd.read_file(in_shp)
    cond = (gdf['DN'] == 1)
    gdf_out = gdf.loc[cond]
    gdf_out.to_file(out_filename)


def main(in_rst_filename, out_boundaries, nodata=0, outmask_rst=None,
         outmask_vect=None):
    """
    Create a vector file of the boudaries of a raster image scene.
    Intermediate step consist in creating a binary mask of the outside of the
    scene, vectorize it and finally extract the inside part of the scene.
    Intermediate can be saved if specified.

    Parameters
    ----------
    in_rst : Raster instance,
        Raster instante of the input raster image.
    out_boundaries : string,
        Path of the output boundarie vector file.
    nodata : float,
        Value that will be set to 0 in the mask. Default to 0.
    outmask_rst : string,
        If indicated, the raster binary mask will be saved at this path.
    outmask_vect : string,
        If indicated, the vector binary mask will be saved at this path.
    """

    print('{} in process ...'.format(os.path.split(in_rst_filename)[1]))

    # create the mask
    out_mask_rst = define_mask(in_rst_filename, nodata,
                               out_filename=outmask_rst)

    # vectorize it
    msk_vect = vectorization(out_mask_rst, out_filename=outmask_vect)

    # extract the inside part of the scene
    extract_boundaries(msk_vect, out_boundaries)

    # delete the intermediate files
    if not outmask_rst:
        os.remove(out_mask_rst)
        print('Temp raster mask deleted...')
    if not outmask_vect:
        os.remove(msk_vect)
        os.remove(os.path.splitext(msk_vect)[0] + '.prj')
        os.remove(os.path.splitext(msk_vect)[0] + '.shx')
        os.remove(os.path.splitext(msk_vect)[0] + '.dbf')
        print('Temp vector mask deleted...')
